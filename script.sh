#!/bin/bash
author="Семенов Алексей"
echo "Программа проверки изменений файла."
echo ""
echo "С помощью данной программы вы можете проверить, изменялся ли дескриптор файла."
echo ""
echo "Разработчик: $author ."
echo ""
current_path=$(pwd)
echo "Текущий каталог: $current_path"
filename=""
while [ -z $filename ]
do
read -p "Введите имя файла: " temp
if [ -e $temp ]
then
filename=$temp
read -p "Введите дату (в формате дд мм гггг чч:мм): " d m y t
if [ ${#d} -lt 2 ]
then
d="0$d"
fi
if [ ${#m} -lt 2 ]
then
m="0$m"
fi
user_date=$(date -d "$y-$m-$d $t" +"%Y-%m-%d %H:%M:%S.%N %z")
user_timestamp=$(date -d "$y-$m-$d $t" +%s)
change_date=$(stat --printf "%z" $filename)
change_timestamp=$(date -d "$change_date" +%s)
echo ""
echo "Введённая дата: $user_date"
echo "Файл был обновлён: $change_date"
echo ""

if [ $user_timestamp -le $change_timestamp ]
then
echo "Индексный дескриптор файла был изменён!"
exit 120
else
echo "Индексный дескриптор файла не был изменён"
exit 0
fi
else
echo "Ошибка - такого файла не существует!"
fi
done

 